//
//  AppDelegate.h
//  JournalAp
//
//  Created by jpeger on 2/5/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSUserDefaults* defaults;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSMutableArray* journalDataArray;

@end

