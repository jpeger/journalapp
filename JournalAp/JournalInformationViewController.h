//
//  JournalInformationViewController.h
//  JournalAp
//
//  Created by jpeger on 2/5/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface JournalInformationViewController : UIViewController<UITextViewDelegate, UITextFieldDelegate>
{
    UITextField* titleField;
    UITextView* tv;
}

@property (nonatomic, strong) NSMutableDictionary* journalDictionary;

@end
