//
//  RootTableViewController.h
//  JournalAp
//
//  Created by jpeger on 2/5/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JournalInformationViewController.h"

@interface RootTableViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    UITableView* entriesView;
    NSMutableArray* entriesArray;
}

@end
