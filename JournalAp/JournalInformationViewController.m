//
//  JournalInformationViewController.m
//  JournalAp
//
//  Created by jpeger on 2/5/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "JournalInformationViewController.h"

@interface JournalInformationViewController ()

@end

@implementation JournalInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    titleField = [[UITextField alloc]initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 40)];
    [titleField setBackgroundColor:[UIColor whiteColor]];
    [titleField setDelegate:self];
    
    tv = [[UITextView alloc]initWithFrame:CGRectMake(10, 60, self.view.frame.size.width-20, self.view.frame.size.height-140)];
    [tv setBackgroundColor:[UIColor whiteColor]];
    [tv setDelegate:self];
    
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"Done Editing" forState:UIControlStateNormal];
    [btn setFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    [btn addTarget:self action:@selector(doneEditing:) forControlEvents:UIControlEventTouchUpInside];
    
    [titleField setInputAccessoryView:btn];
    [tv setInputAccessoryView:btn];
    
    [self.view setBackgroundColor:[UIColor grayColor]];
    [self.view addSubview:titleField];
    [self.view addSubview:tv];
    
    if (!self.journalDictionary) {
        self.journalDictionary = [[NSMutableDictionary alloc]init];
    }else{
        tv.text=[self.journalDictionary objectForKey:@"tv"];
        titleField.text=[self.journalDictionary objectForKey:@"titleField"];
    }
}

-(void)doneEditing:(id)sender {
    [self.view endEditing:YES];
    
    NSDateFormatter* curDate = [[NSDateFormatter alloc]init];
    [curDate setDateStyle:NSDateFormatterShortStyle];
    
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    [self.journalDictionary setObject:tv.text forKey:@"tv"];
    [self.journalDictionary setObject:titleField.text forKey:@"titleField"];
    [self.journalDictionary setObject:[curDate stringFromDate:[NSDate date]] forKey:@"journalDate"];

    
    [ad.journalDataArray addObject:self.journalDictionary];
    
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    tv.frame = CGRectMake(10, 60, self.view.frame.size.width - 20, self.view.frame.size.height/2 - 60);
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    tv.frame = CGRectMake(10, 60, self.view.frame.size.width-20, self.view.frame.size.height-80);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    tv.frame = CGRectMake(10, 60, self.view.frame.size.width - 20, self.view.frame.size.height/2 - 60);
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    tv.frame = CGRectMake(10, 60, self.view.frame.size.width-20, self.view.frame.size.height-80);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
