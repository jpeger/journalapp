//
//  RootTableViewController.m
//  JournalAp
//
//  Created by jpeger on 2/5/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "RootTableViewController.h"
#import "AppDelegate.h"

@interface RootTableViewController ()

@end

@implementation RootTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIBarButtonItem* addEntryBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(newEntry:)];
    
    self.navigationItem.rightBarButtonItem = addEntryBtn;
    entriesView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [entriesView setDataSource:self];
    [entriesView setDelegate:self];
    [self.view addSubview:entriesView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadData];
}

-(void)newEntry:(id)sender{
    JournalInformationViewController* jivc = [JournalInformationViewController new];
    [self.navigationController pushViewController:jivc animated:YES];
}

-(void)loadData{
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    entriesArray = ad.journalDataArray;
    
    [entriesView reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    NSDictionary* d = [entriesArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [d objectForKey:@"journalDate"];
    cell.detailTextLabel.text = [d objectForKey:@"titleField"];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [entriesView deselectRowAtIndexPath:indexPath animated:YES];
    
    JournalInformationViewController* jivc = [JournalInformationViewController new];
    
    jivc.journalDictionary = [entriesArray objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:jivc animated:YES];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return entriesArray.count;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
