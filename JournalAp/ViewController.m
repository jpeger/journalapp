//
//  ViewController.m
//  JournalAp
//
//  Created by jpeger on 2/5/15.
//  Copyright (c) 2015 jpeger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    RootTableViewController* rtvc = [RootTableViewController new];
    
    UINavigationController* nc = [[UINavigationController alloc]initWithRootViewController:rtvc];
    
    [nc.navigationBar setTranslucent:NO];
    [self addChildViewController:nc];
    [self.view addSubview:nc.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
